unsigned char A = 5;		A: 1000
							Load LoadR0, #5		Load-> Imediato
							Store 1000, R0	-> Direto

unsigned char B = 6;		B: 1001
							Load R0, #6		-> Imediato
							Store 1001, R0	-> Direto

unsigned char C;			C: 1002

C = A + B;					Load R0, 1000	-> Direto
							Load R1, 1001	-> Direto
							ADD R2, R0, R1
							Store 1002, R2




// gcc -O3 march=core2 -ojhkj -lm -Wall -Wextra


Assembly:			Binary:

Load R0, #5			111 00000101 00
Store 1000, R0	
Load R0, #6		
Store 1001, R0	
Load R0, 1000	
Load R1, 1001	
ADD R2, R0, R1
Store 1002, R2

Load Rx End			011 eeeeeeee rr
Load R0, #5			111 nnnnnnnn rr
Store End Rx		100 eeeeeeee rr
Add Rx, Rx, Rx		101 rr rr rr xxxx
Mul Rx, Rx, Rx		110 rr rr rr xxxx



Imediato: Load R0, #5
Direto:   Load R0, 5
Indireto por Registrador:  Load R0, R1  R0 <- M[R1]
Indireto: Load R0, *6    				R0 <- M[M[6]]
Indireto: Load R0, **7   				R0 <- M[M[M[7]]]


Mov R0, R1


0: sdf
1: sdf
2: sdf
3: sdf
4: sdf
5: 1
6: 5
7: 6
8
9
